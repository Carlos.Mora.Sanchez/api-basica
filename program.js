//REST 

import express from 'express';

let app= new express();

//RUTAS 

function saludar(peticion, respuesta){

    respuesta.send("Hola API!!");
}

app.get('/saludo', saludar);

//RUTAS 

function calcular(req, res){

    let miSumador=new Sumador();

    let resultado=miSumador.sumar(78,78);

    //Devolver el resultado de una operacion
    
    res.send(resultado.toString())
}

app.get('/calculadora', calcular);


//Encender el servidor de la API

app.listen(3000);

class Sumador{

    sumar(a,b){

        return a+b;
    }
}
